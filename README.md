# Creditz
Creditz is a tool that scans your dependencies during build (with the
help of the
[Gradle License Report Plugin](https://github.com/jk1/Gradle-License-Report)),
makes the information about your dependencies available at runtime and
provides a simple activity for displaying them along with some
information about the app itself.

_Note: Creditz makes the dependency information available by writing it
to a generated Kotlin source file. Because of this your build
environment will need to be able to compile Kotlin code in order to use
Creditz._

# Usage
Add repositories for JitPack and Gradle plugins to Gradle and make the
Creditz plugin available on your classpath:
````groovy
buildscript {
    repositories {
        // ...
        maven { url "https://jitpack.io" }
        maven { url "https://plugins.gradle.org/m2/" }
    }
    
    dependencies {
        // ...
        classpath 'com.gitlab.Namnodorel.Creditz:generator:$latestCreditzVersion'
    }
}
````
Apply the plugin and add the Android activity: 
````groovy
apply plugin: 'de.namnodorel.creditz.generator'

// ...

dependencies {
    implementation 'com.gitlab.Namnodorel.Creditz:views:$latestCreditzVersion'
}

````

Use it in your app: 
````kotlin
// Class for configuring the about page. Using it is entirely optional.
// Anything that you do not explicitly configure either uses a default
// or hides the relevant view.
val config = CreditzConfig().apply {
    // Set the "group mode" the about page will use. There are three options:
    //      DIRECT: Only show direct dependencies.
    //      DIRECT_TRANSITIVE_SEPERATE: Show direct and transitive dependencies,
    //          but each in their own category.
    //      DIRECT_TRANSITIVE_MERGED: Show direct and transitive dependencies
    //          in one unified list.
    groupMode = GroupMode.DIRECT_TRANSITIVE_SEPERATE
    
    // Configure a custom theme that the activity will use.
    // Applies only if you use the provided activity (the alternative would be the fragment)
    // Must be a descendant of a material-theme
    theme = myThemeResId
    
    // Configure a title at the top of the about page, usually your apps name.
    aboutTitle = "My App"
    
    // Configure a description to display at the top of the about page.
    aboutDescription = "An app made by humans, for humans, with humans."
    
    contributors = listOf(
        ContributorInfo("Mister X", "Added a mysterious flare to the whole project", "https://example.com")
    )
    
    // Configure any amount of additional buttons that will be displayed below
    // the description.
    config.infoButtons = listOf(
        // Open a link when the button gets clicked
        InfoButton("Project Page", "https://example.com"),
        // Start an intent to send a mail to the specified address
        InfoButton("Report a Bug", "mailto:e@mail.com")
    )
}

// Start as a new activity
val intent = Intent(context, AboutActivity::class.java)
intent.putExtra(AboutActivity.CREDITZ_CONFIG, config)
startActivity(intent)

// Or just create a fragment
val aboutFrag = AboutFragment()

val args = Bundle()
args.putParcelable(AboutActivity.CREDITZ_CONFIG, config)
aboutFrag.arguments = args
// Do something with the fragment
````

You can also use the fragment with the Android Navigation library - Android Studio may complain in the
editor that it can't find the fragment, but this still works as expected in practice:
````xml
<fragment
    android:id="@+id/aboutFragment"
    android:name="de.namnodorel.creditz.views.AboutFragment">

    <argument
        android:name="config"
        android:defaultValue="@null"
        app:argType="de.namnodorel.creditz.views.CreditzConfig"
        app:nullable="true" />

</fragment>
````

# Bugs/Feature Requests
If you find a bug or want to request a feature, please first check
whether someone already created an issue for it. If not, you can do so
[here](https://gitlab.com/Namnodorel/creditz/issues).

# License
````
Copyright 2019 Felix Günther (Namnodorel)
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
````