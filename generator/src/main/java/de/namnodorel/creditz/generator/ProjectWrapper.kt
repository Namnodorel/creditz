/*
 *    Copyright 2019 Felix Günther (Namnodorel)
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package de.namnodorel.creditz.generator

import com.github.jk1.license.LicenseReportExtension
import groovy.lang.Closure
import groovy.lang.GroovyObjectSupport
import org.gradle.api.*
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.artifacts.dsl.ArtifactHandler
import org.gradle.api.artifacts.dsl.DependencyHandler
import org.gradle.api.artifacts.dsl.DependencyLockingHandler
import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.api.component.SoftwareComponentContainer
import org.gradle.api.file.*
import org.gradle.api.initialization.dsl.ScriptHandler
import org.gradle.api.invocation.Gradle
import org.gradle.api.logging.Logger
import org.gradle.api.logging.LoggingManager
import org.gradle.api.model.ObjectFactory
import org.gradle.api.plugins.*
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.gradle.api.resources.ResourceHandler
import org.gradle.api.tasks.TaskContainer
import org.gradle.api.tasks.WorkResult
import org.gradle.normalization.InputNormalizationHandler
import org.gradle.process.ExecResult
import org.gradle.process.ExecSpec
import org.gradle.process.JavaExecSpec
import java.io.File
import java.net.URI
import java.util.concurrent.Callable


class ProjectWrapper(private val returnConfig: LicenseReportExtension) : Project, GroovyObjectSupport() {

    override fun getProperty(property: String?): Any {
        return returnConfig
    }

    // None of the methods are implemented, because this is just a stub for feeding the normalizer filter
    // the config (which _should_ be injected, but well... is not)

    override fun getGroup(): Any {
        TODO("not implemented")
    }

    override fun afterEvaluate(p0: Action<in Project>) {
        TODO("not implemented")
    }

    override fun afterEvaluate(p0: Closure<Any>) {
        TODO("not implemented")
    }

    override fun getDefaultTasks(): MutableList<String> {
        TODO("not implemented")
    }

    override fun getConvention(): Convention {
        TODO("not implemented")
    }

    override fun getLogger(): Logger {
        TODO("not implemented")
    }

    override fun getBuildDir(): File {
        TODO("not implemented")
    }

    override fun getAnt(): AntBuilder {
        TODO("not implemented")
    }

    override fun getVersion(): Any {
        TODO("not implemented")
    }

    override fun getRootProject(): Project {
        TODO("not implemented")
    }

    override fun depthCompare(p0: Project): Int {
        TODO("not implemented")
    }

    override fun getGradle(): Gradle {
        TODO("not implemented")
    }

    override fun getAllTasks(p0: Boolean): MutableMap<Project, MutableSet<Task>> {
        TODO("not implemented")
    }

    override fun uri(p0: Any): URI {
        TODO("not implemented")
    }

    override fun copySpec(p0: Closure<Any>): CopySpec {
        TODO("not implemented")
    }

    override fun copySpec(p0: Action<in CopySpec>): CopySpec {
        TODO("not implemented")
    }

    override fun copySpec(): CopySpec {
        TODO("not implemented")
    }

    override fun relativePath(p0: Any): String {
        TODO("not implemented")
    }

    override fun setProperty(p0: String, p1: Any?) {
        TODO("not implemented")
    }

    override fun beforeEvaluate(p0: Action<in Project>) {
        TODO("not implemented")
    }

    override fun beforeEvaluate(p0: Closure<Any>) {
        TODO("not implemented")
    }

    override fun property(p0: String): Any? {
        TODO("not implemented")
    }

    override fun buildscript(p0: Closure<Any>) {
        TODO("not implemented")
    }

    override fun getProject(): Project {
        TODO("not implemented")
    }

    override fun dependencies(p0: Closure<Any>) {
        TODO("not implemented")
    }

    override fun getPath(): String {
        TODO("not implemented")
    }

    override fun zipTree(p0: Any): FileTree {
        TODO("not implemented")
    }

    override fun allprojects(p0: Action<in Project>) {
        TODO("not implemented")
    }

    override fun allprojects(p0: Closure<Any>) {
        TODO("not implemented")
    }

    override fun <T : Any?> container(p0: Class<T>): NamedDomainObjectContainer<T> {
        TODO("not implemented")
    }

    override fun <T : Any?> container(p0: Class<T>, p1: NamedDomainObjectFactory<T>): NamedDomainObjectContainer<T> {
        TODO("not implemented")
    }

    override fun <T : Any?> container(p0: Class<T>, p1: Closure<Any>): NamedDomainObjectContainer<T> {
        TODO("not implemented")
    }

    override fun repositories(p0: Closure<Any>) {
        TODO("not implemented")
    }

    override fun evaluationDependsOnChildren() {
        TODO("not implemented")
    }

    override fun configure(p0: Any, p1: Closure<Any>): Any {
        TODO("not implemented")
    }

    override fun configure(p0: MutableIterable<*>, p1: Closure<Any>): MutableIterable<*> {
        TODO("not implemented")
    }

    override fun <T : Any?> configure(p0: MutableIterable<T>, p1: Action<in T>): MutableIterable<T> {
        TODO("not implemented")
    }

    override fun exec(p0: Closure<Any>): ExecResult {
        TODO("not implemented")
    }

    override fun exec(p0: Action<in ExecSpec>): ExecResult {
        TODO("not implemented")
    }

    override fun sync(p0: Action<in CopySpec>): WorkResult {
        TODO("not implemented")
    }

    override fun configurations(p0: Closure<Any>) {
        TODO("not implemented")
    }

    override fun getExtensions(): ExtensionContainer {
        TODO("not implemented")
    }

    override fun getProperties(): MutableMap<String, *> {
        TODO("not implemented")
    }

    override fun absoluteProjectPath(p0: String): String {
        TODO("not implemented")
    }

    override fun getProjectDir(): File {
        TODO("not implemented")
    }

    override fun files(vararg p0: Any?): ConfigurableFileCollection {
        TODO("not implemented")
    }

    override fun files(p0: Any, p1: Closure<Any>): ConfigurableFileCollection {
        TODO("not implemented")
    }

    override fun files(p0: Any, p1: Action<in ConfigurableFileCollection>): ConfigurableFileCollection {
        TODO("not implemented")
    }

    override fun hasProperty(p0: String): Boolean {
        TODO("not implemented")
    }

    override fun getState(): ProjectState {
        TODO("not implemented")
    }

    override fun getComponents(): SoftwareComponentContainer {
        TODO("not implemented")
    }

    override fun setBuildDir(p0: File) {
        TODO("not implemented")
    }

    override fun setBuildDir(p0: Any) {
        TODO("not implemented")
    }

    override fun defaultTasks(vararg p0: String?) {
        TODO("not implemented")
    }

    override fun compareTo(other: Project?): Int {
        TODO("not implemented")
    }

    override fun artifacts(p0: Closure<Any>) {
        TODO("not implemented")
    }

    override fun artifacts(p0: Action<in ArtifactHandler>) {
        TODO("not implemented")
    }

    override fun getRootDir(): File {
        TODO("not implemented")
    }

    override fun getDependencyLocking(): DependencyLockingHandler {
        TODO("not implemented")
    }

    override fun <T : Any?> provider(p0: Callable<T>): Provider<T> {
        TODO("not implemented")
    }

    override fun findProperty(p0: String): Any? {
        TODO("not implemented")
    }

    override fun getDependencies(): DependencyHandler {
        TODO("not implemented")
    }

    override fun getResources(): ResourceHandler {
        TODO("not implemented")
    }

    override fun setDefaultTasks(p0: MutableList<String>) {
        TODO("not implemented")
    }

    override fun normalization(p0: Action<in InputNormalizationHandler>) {
        TODO("not implemented")
    }

    override fun project(p0: String): Project {
        TODO("not implemented")
    }

    override fun project(p0: String, p1: Closure<Any>): Project {
        TODO("not implemented")
    }

    override fun project(p0: String, p1: Action<in Project>): Project {
        TODO("not implemented")
    }

    override fun task(p0: String): Task {
        TODO("not implemented")
    }

    override fun task(p0: MutableMap<String, *>, p1: String): Task {
        TODO("not implemented")
    }

    override fun task(p0: MutableMap<String, *>, p1: String, p2: Closure<Any>): Task {
        TODO("not implemented")
    }

    override fun task(p0: String, p1: Closure<Any>): Task {
        TODO("not implemented")
    }

    override fun task(p0: String, p1: Action<in Task>): Task {
        TODO("not implemented")
    }

    override fun copy(p0: Closure<Any>): WorkResult {
        TODO("not implemented")
    }

    override fun copy(p0: Action<in CopySpec>): WorkResult {
        TODO("not implemented")
    }

    override fun getDescription(): String? {
        TODO("not implemented")
    }

    override fun subprojects(p0: Action<in Project>) {
        TODO("not implemented")
    }

    override fun subprojects(p0: Closure<Any>) {
        TODO("not implemented")
    }

    override fun getBuildscript(): ScriptHandler {
        TODO("not implemented")
    }

    override fun getStatus(): Any {
        TODO("not implemented")
    }

    override fun mkdir(p0: Any): File {
        TODO("not implemented")
    }

    override fun setStatus(p0: Any) {
        TODO("not implemented")
    }

    override fun getConfigurations(): ConfigurationContainer {
        TODO("not implemented")
    }

    override fun getArtifacts(): ArtifactHandler {
        TODO("not implemented")
    }

    override fun setDescription(p0: String?) {
        TODO("not implemented")
    }

    override fun getLayout(): ProjectLayout {
        TODO("not implemented")
    }

    override fun apply(p0: Closure<Any>) {
        TODO("not implemented")
    }

    override fun apply(p0: Action<in ObjectConfigurationAction>) {
        TODO("not implemented")
    }

    override fun apply(p0: MutableMap<String, *>) {
        TODO("not implemented")
    }

    override fun evaluationDependsOn(p0: String): Project {
        TODO("not implemented")
    }

    override fun javaexec(p0: Closure<Any>): ExecResult {
        TODO("not implemented")
    }

    override fun javaexec(p0: Action<in JavaExecSpec>): ExecResult {
        TODO("not implemented")
    }

    override fun getChildProjects(): MutableMap<String, Project> {
        TODO("not implemented")
    }

    override fun getLogging(): LoggingManager {
        TODO("not implemented")
    }

    override fun getTasks(): TaskContainer {
        TODO("not implemented")
    }

    override fun getName(): String {
        TODO("not implemented")
    }

    override fun file(p0: Any): File {
        TODO("not implemented")
    }

    override fun file(p0: Any, p1: PathValidation): File {
        TODO("not implemented")
    }

    override fun findProject(p0: String): Project? {
        TODO("not implemented")
    }

    override fun getPlugins(): PluginContainer {
        TODO("not implemented")
    }

    override fun ant(p0: Closure<Any>): AntBuilder {
        TODO("not implemented")
    }

    override fun ant(p0: Action<in AntBuilder>): AntBuilder {
        TODO("not implemented")
    }

    override fun getAllprojects(): MutableSet<Project> {
        TODO("not implemented")
    }

    override fun createAntBuilder(): AntBuilder {
        TODO("not implemented")
    }

    override fun getObjects(): ObjectFactory {
        TODO("not implemented")
    }

    override fun dependencyLocking(p0: Action<in DependencyLockingHandler>) {
        TODO("not implemented")
    }

    override fun tarTree(p0: Any): FileTree {
        TODO("not implemented")
    }

    override fun delete(vararg p0: Any?): Boolean {
        TODO("not implemented")
    }

    override fun delete(p0: Action<in DeleteSpec>): WorkResult {
        TODO("not implemented")
    }

    override fun getRepositories(): RepositoryHandler {
        TODO("not implemented")
    }

    override fun getTasksByName(p0: String, p1: Boolean): MutableSet<Task> {
        TODO("not implemented")
    }

    override fun getParent(): Project? {
        TODO("not implemented")
    }

    override fun getDisplayName(): String {
        TODO("not implemented")
    }

    override fun relativeProjectPath(p0: String): String {
        TODO("not implemented")
    }

    override fun getPluginManager(): PluginManager {
        TODO("not implemented")
    }

    override fun setGroup(p0: Any) {
        TODO("not implemented")
    }

    override fun fileTree(p0: Any): ConfigurableFileTree {
        TODO("not implemented")
    }

    override fun fileTree(p0: Any, p1: Closure<Any>): ConfigurableFileTree {
        TODO("not implemented")
    }

    override fun fileTree(p0: Any, p1: Action<in ConfigurableFileTree>): ConfigurableFileTree {
        TODO("not implemented")
    }

    override fun fileTree(p0: MutableMap<String, *>): ConfigurableFileTree {
        TODO("not implemented")
    }

    override fun getNormalization(): InputNormalizationHandler {
        TODO("not implemented")
    }

    override fun setVersion(p0: Any) {
        TODO("not implemented")
    }

    override fun getDepth(): Int {
        TODO("not implemented")
    }

    override fun getProviders(): ProviderFactory {
        TODO("not implemented")
    }

    override fun getSubprojects(): MutableSet<Project> {
        TODO("not implemented")
    }

    override fun getBuildFile(): File {
        TODO("not implemented")
    }
}