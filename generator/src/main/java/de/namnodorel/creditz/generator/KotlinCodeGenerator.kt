/*
 *    Copyright 2019 Felix Günther (Namnodorel)
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package de.namnodorel.creditz.generator

import com.github.jk1.license.ProjectData
import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import java.io.File


class KotlinCodeGenerator(private val outputDir: File) {

    fun generateCodeFor(allDependencyData: ProjectData, firstLevelDependencyData: ProjectData) {

        val dependencyInfo = collectDependencyData(allDependencyData, firstLevelDependencyData)

        val fileSpec = generateInfoStoreFile(dependencyInfo)
        fileSpec.writeTo(outputDir)
    }

    private fun collectDependencyData(allDependencyData: ProjectData, firstLevelDependencyData: ProjectData): Set<DependencyData> {
        val collectedInfo = HashSet<DependencyData>()

        allDependencyData.allDependencies.forEach { dependency ->

            val transitive = !firstLevelDependencyData.allDependencies.any {
                it.group == dependency.group
                        && it.name == dependency.name
                        && it.version == dependency.version
            }

            val dependencyData = DependencyData(dependency.group, dependency.name, dependency.version, transitive)

            if(dependency.manifests.isNotEmpty()){
                dependency.manifests.forEach { manifest ->

                    dependencyData.apply {
                        name = manifest.name
                        version = manifest.version
                        description = manifest.description
                        website = manifest.url
                        if(manifest.vendor != null) {
                            authors.add(manifest.vendor)
                        }
                        addLicense(manifest.license, manifest.licenseUrl)
                    }

                }
            }

            if(dependency.poms.isNotEmpty()){
                dependency.poms.forEach {  pom ->

                    dependencyData.apply {
                        name = pom.name
                        description = pom.description
                        website = pom.projectUrl
                        addLicenses(pom.licenses)
                        authors.addAll(pom.developers.map { it.name })
                    }

                }

            }

            if(dependency.licenseFiles.isNotEmpty()){
                dependency.licenseFiles
                    .flatMap { it.fileDetails }
                    .forEach { licenseFile ->

                        dependencyData.apply {
                            addLicense(licenseFile.license, licenseFile.licenseUrl)
                        }

                    }
            }

            collectedInfo.add(dependencyData)
        }
        return collectedInfo
    }

    private fun generateInfoStoreFile(dependencyInfo: Set<DependencyData>): FileSpec {

        val infoStoreType = generateInfoStoreType(dependencyInfo)

        val file = FileSpec.builder(DEPENDENCY_INFO_PACKAGE, DEPENDENCY_INFO_STORE_CLASS)
            .addType(infoStoreType)

        return file.build()
    }

    private fun generateInfoStoreType(dependencyInfo: Set<DependencyData>): TypeSpec {

        val buildFunc = generateBuildFunction(dependencyInfo)

        val type = TypeSpec.classBuilder(DEPENDENCY_INFO_STORE_CLASS)
            .addFunction(buildFunc)

        return type.build()
    }


    private fun generateBuildFunction(dependencyInfo: Set<DependencyData>): FunSpec {

        val buildFun = FunSpec.builder("buildInfo")
            .addModifiers(KModifier.PUBLIC)
            .returns(DEPENDENCY_INFO_COLLECTION_CLASS)
            .addStatement("val info = HashSet<%T>()", DEPENDENCY_INFO_CLASS)

        for(depData in dependencyInfo){

            val stringArgs = HashMap<String, Any>()
            stringArgs["depInfoType"] = DEPENDENCY_INFO_CLASS
            stringArgs["moduleGroup"] = depData.moduleGroup
            stringArgs["moduleName"] = depData.moduleName
            stringArgs["moduleVersion"] = depData.moduleVersion
            stringArgs["transitive"] = depData.transitive.toString()
            stringArgs["name"] = depData.name ?: ""
            stringArgs["version"] = depData.version ?: ""
            stringArgs["description"] = depData.description ?: ""
            stringArgs["website"] = depData.website ?: ""

            var authorsString = "emptySet()"
            if(depData.authors.isNotEmpty()){
                authorsString = "setOf("
                for(author in depData.authors){
                    val index = depData.authors.indexOf(author)
                    authorsString += "%author$index:S,"
                    stringArgs["author$index"] = author
                }
                authorsString = authorsString.dropLast(1)
                authorsString += ")"
            }

            var licensesString = "emptyMap()"
            if(depData.licenses.isNotEmpty()){
                licensesString = "mapOf("
                for(license in depData.licenses){

                    val index = depData.licenses.indexOf(license)
                    licensesString += "Pair(%licenseName$index:S,%licenseUrl$index:S),"

                    stringArgs["licenseName$index"] = license.name ?: "Unidentified License"
                    stringArgs["licenseUrl$index"] = license.url ?: "no_url"
                }
                licensesString = licensesString.dropLast(1)
                licensesString += ")"
            }

            buildFun.addCode(CodeBlock.builder().addNamed("""
                info.add(%depInfoType:T(
                    %moduleGroup:S,
                    %moduleName:S,
                    %moduleVersion:S,
                    %transitive:L,
                    %name:S,
                    %version:S,
                    %description:S,
                    %website:S,
                    $authorsString,
                    $licensesString
                ))
                
            """.trimIndent(), stringArgs).build())

        }

        buildFun.addStatement("return info")

        return buildFun.build()
    }

    companion object {
        private val DEPENDENCY_INFO_CLASS = ClassName("de.namnodorel.creditz.views", "DependencyInfo")
        private val DEPENDENCY_INFO_COLLECTION_CLASS = SET.parameterizedBy(DEPENDENCY_INFO_CLASS)

        private const val DEPENDENCY_INFO_PACKAGE = "de.namnodorel.creditz.info"
        private const val DEPENDENCY_INFO_STORE_CLASS = "DependencyInfoStore"
    }
}