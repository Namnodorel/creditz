/*
 *    Copyright 2019 Felix Günther (Namnodorel)
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package de.namnodorel.creditz.generator

import com.github.jk1.license.License

class DependencyData(val moduleGroup: String, val moduleName: String, val moduleVersion: String, val transitive: Boolean){

    var name: String? = null
        set(value) {
            field = if(value != null && value.isNotEmpty()) value else field
        }

    var version: String? = null
        set(value) {
            field = if(value != null && value.isNotEmpty()) value else field
        }

    var description: String? = null
        set(value) {
            field = if(value != null && value.isNotEmpty()) value else field
        }

    var website: String? = null
        set(value) {
            field = if(value != null && value.isNotEmpty()) value else field
        }

    val authors: MutableSet<String> = HashSet()
    val licenses: MutableSet<License> = HashSet()

    fun addLicense(licenseName: String?, licenseUrl: String?){
        if((licenseName != null && licenseName.isNotEmpty())
            || (licenseUrl != null && licenseUrl.isNotEmpty())){
            licenses.add(License(licenseName ?: "Unidentified License", licenseUrl ?: "no_url"))
        }
    }

    fun addLicenses(licensesToAdd: Iterable<License>){
        licensesToAdd.forEach { license ->
            if((license.name != null && license.name.isNotEmpty())
                || (license.url != null && license.url.isNotEmpty())){
                licenses.add(license)
            }
        }
    }
}