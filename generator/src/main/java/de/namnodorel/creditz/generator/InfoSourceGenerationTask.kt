/*
 *    Copyright 2019 Felix Günther (Namnodorel)
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package de.namnodorel.creditz.generator

import com.github.jk1.license.LicenseReportExtension
import com.github.jk1.license.filter.ExcludeTransitiveDependenciesFilter
import com.github.jk1.license.filter.LicenseBundleNormalizer
import com.github.jk1.license.filter.ReduceDuplicateLicensesFilter
import com.github.jk1.license.reader.ProjectReader
import org.gradle.api.DefaultTask
import org.gradle.api.file.ConfigurableFileCollection
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.*
import java.io.File

@CacheableTask
open class InfoSourceGenerationTask : DefaultTask() {

    init {
        group = "Build"
        description = "Generates Kotlin classes with meta information on all dependencies"
    }

    private val config = LicenseReportExtension(project)
    private val outputDir = project.buildDir.toString() + LICENSE_INFO_DIR
    private val codeDir = project.buildDir.toString() + GENERATED_SOURCES_DIR

    @InputFiles
    @PathSensitive(value = PathSensitivity.ABSOLUTE)
    fun getClasspath(): FileCollection {
        return config.projects
            .flatMap { ProjectReader.findConfiguredConfigurations(it, config) }
            .foldRight(project.files() as FileCollection){ configurations, fileCollection ->
                return@foldRight fileCollection.plus(configurations)
            }
    }

    @OutputDirectories
    fun getOutputDirs(): ConfigurableFileCollection {
        return project.files(File(outputDir), File(codeDir))
    }

    @TaskAction
    fun execute(){

        config.outputDir = outputDir
        config.configurations = emptyArray()

        var allDependencyData = ProjectReader(config).read(project)
        val firstLevelDependencyData = ExcludeTransitiveDependenciesFilter().filter(allDependencyData)

        allDependencyData.project = ProjectWrapper(config)
        allDependencyData = LicenseBundleNormalizer().filter(allDependencyData)
        allDependencyData = ReduceDuplicateLicensesFilter().filter(allDependencyData)

        KotlinCodeGenerator(File(codeDir)).generateCodeFor(allDependencyData, firstLevelDependencyData)

    }

    companion object {
        const val LICENSE_INFO_DIR = "/dependency_license_info"
        const val GENERATED_SOURCES_DIR = "/generated/source/dependencyInfo"
    }
}