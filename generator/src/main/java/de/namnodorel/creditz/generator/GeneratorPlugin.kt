/*
 *    Copyright 2019 Felix Günther (Namnodorel)
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package de.namnodorel.creditz.generator

import com.android.build.gradle.BaseExtension
import org.gradle.api.Plugin
import org.gradle.api.Project

class GeneratorPlugin : Plugin<Project>{
    override fun apply(project: Project) {
        val tasks = project.tasks

        val task = tasks.create("generateDependencyInfoSource", InfoSourceGenerationTask::class.java)

        (project.extensions.getByName("android") as BaseExtension).sourceSets {
            it.named("main").get().java.srcDir(project.buildDir.toString() + InfoSourceGenerationTask.GENERATED_SOURCES_DIR)
        }

        project.afterEvaluate {
            tasks.findByName("compileKotlin")?.dependsOn(task)
            tasks.findByName("compileDebugKotlin")?.dependsOn(task)
            tasks.findByName("compileDebugAndroidTestKotlin")?.dependsOn(task)
            tasks.findByName("compileDebugUnitTestKotlin")?.dependsOn(task)
            tasks.findByName("compileReleaseKotlin")?.dependsOn(task)
            tasks.findByName("compileReleaseUnitTestKotlin")?.dependsOn(task)
        }


    }

}
