/*
 *    Copyright 2019 Felix Günther (Namnodorel)
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package de.namnodorel.creditz.views

import android.os.Parcelable
import androidx.annotation.StyleRes
import kotlinx.android.parcel.Parcelize
import java.util.*


@Parcelize
data class CreditzConfig(
    var aboutTitle: String? = null,
    var aboutDescription: String? = null,
    @StyleRes var theme: Int? = null,
    var infoButtons: List<InfoButton> = LinkedList(),
    var contributors: List<ContributorInfo> = LinkedList(),
    var groupMode: GroupMode = GroupMode.DIRECT_TRANSITIVE_SEPERATE,
    var linkToProjects: Boolean = true
) : Parcelable