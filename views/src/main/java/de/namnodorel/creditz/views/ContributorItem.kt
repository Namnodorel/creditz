/*
 *    Copyright 2019 Felix Günther (Namnodorel)
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package de.namnodorel.creditz.views

import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import de.namnodorel.creditz.R


class ContributorItem(val contributorInfo: ContributorInfo) : AbstractItem<ContributorItem.ViewHolder>() {

    override val layoutRes: Int
        get() = R.layout.contributor_item

    override val type: Int
        get() = R.id.contributor_info_item

    override fun getViewHolder(v: View): ViewHolder = ViewHolder(v as ConstraintLayout)

    class ViewHolder(private val view: ConstraintLayout) : FastAdapter.ViewHolder<ContributorItem>(view) {

        override fun bindView(item: ContributorItem, payloads: List<Any>) {
            val info = item.contributorInfo

            val nameTv = view.findViewById<TextView>(R.id.contributor_name)
            nameTv.text = info.name

            val subtextTv = view.findViewById<TextView>(R.id.contributor_subtext)
            if(info.subtext != null){
                subtextTv.text = info.subtext
            }

            if(info.goTo != null){
                view.setOnClickListener {
                    try {

                        val intent = Intent(Intent.ACTION_VIEW)
                        intent.data = Uri.parse(info.goTo)
                        view.context.startActivity(intent)

                    } catch (ex: Exception){
                        Toast.makeText(view.context, "Failed to open", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }

        override fun unbindView(item: ContributorItem) {
            val subtextTv = view.findViewById<TextView>(R.id.contributor_subtext)
            subtextTv.text = ""

            view.setOnClickListener {  }
        }

    }
}