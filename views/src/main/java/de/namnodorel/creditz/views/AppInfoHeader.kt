/*
 *    Copyright 2019 Felix Günther (Namnodorel)
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package de.namnodorel.creditz.views

import android.content.Intent
import android.content.Intent.ACTION_SENDTO
import android.content.Intent.ACTION_VIEW
import android.net.Uri
import android.view.View
import android.view.View.GONE
import android.widget.TextView
import android.widget.Toast
import com.google.android.flexbox.FlexboxLayout
import com.google.android.material.button.MaterialButton
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import de.namnodorel.creditz.R


class AppInfoHeader(
    val aboutTitle: String?,
    val aboutDescription: String?,
    val infoButtons: List<InfoButton>
) : AbstractItem<AppInfoHeader.ViewHolder>() {

    override val layoutRes: Int
        get() = R.layout.app_info

    override val type: Int
        get() = 0

    override fun getViewHolder(v: View): ViewHolder = ViewHolder(v)


    class ViewHolder(private val view: View) : FastAdapter.ViewHolder<AppInfoHeader>(view) {

        override fun bindView(item: AppInfoHeader, payloads: List<Any>) {

            val titleTv = view.findViewById<TextView>(R.id.app_info_title)
            if(!item.aboutTitle.isNullOrEmpty()){
                titleTv.text = item.aboutTitle
            }else{
                titleTv.visibility = GONE
            }

            val descriptionTv = view.findViewById<TextView>(R.id.app_info_description)
            if(!item.aboutDescription.isNullOrEmpty()){
                descriptionTv.text = item.aboutDescription
            }else{
                descriptionTv.visibility = GONE
            }

            val infoButtonLayout = view.findViewById<FlexboxLayout>(R.id.info_buttons)
            for(infoBtn in item.infoButtons){

                val button = MaterialButton(view.context)
                button.text = infoBtn.title
                button.setOnClickListener {

                    try {

                        val intent = Intent(ACTION_VIEW)
                        intent.data = Uri.parse(infoBtn.goTo)
                        view.context.startActivity(intent)

                    } catch (ex: Exception){
                        Toast.makeText(view.context, "Failed to open", Toast.LENGTH_SHORT).show()
                    }

                }

                infoButtonLayout.addView(button)
            }
        }

        override fun unbindView(item: AppInfoHeader) {
            val infoButtonLayout = view.findViewById<FlexboxLayout>(R.id.info_buttons)
            infoButtonLayout.removeAllViews()
        }

    }

}