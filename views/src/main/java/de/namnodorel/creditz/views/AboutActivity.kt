/*
 *    Copyright 2019 Felix Günther (Namnodorel)
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package de.namnodorel.creditz.views

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import de.namnodorel.creditz.R


class AboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val config = intent.getParcelableExtra(CREDITZ_CONFIG) ?: CreditzConfig()

        if(config.theme != null){
            setTheme(config.theme!!)
        }

        setContentView(R.layout.activity_about)

        if(savedInstanceState == null){
            val aboutFrag = AboutFragment()

            val args = Bundle()
            args.putParcelable(CREDITZ_CONFIG, config)
            aboutFrag.arguments = args

            val transaction = supportFragmentManager.beginTransaction()
            transaction
                .add(R.id.about_fragment_container, aboutFrag)
                .commit()
        }

    }

    companion object {
        const val CREDITZ_CONFIG = "config"
    }
}