/*
 *    Copyright 2019 Felix Günther (Namnodorel)
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package de.namnodorel.creditz.views

import android.view.View
import android.widget.TextView
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import de.namnodorel.creditz.R


class AboutCategory(val categoryTitle: String) : AbstractItem<AboutCategory.ViewHolder>() {

    override val layoutRes: Int
        get() = R.layout.dependency_category

    override val type: Int
        get() = R.id.about_category_item

    override fun getViewHolder(v: View): ViewHolder = ViewHolder(v)


    class ViewHolder(private val view: View) : FastAdapter.ViewHolder<AboutCategory>(view) {

        override fun bindView(item: AboutCategory, payloads: List<Any>) {

            val categoryTitleTv = view.findViewById<TextView>(R.id.category_title)

            categoryTitleTv.text = item.categoryTitle

        }

        override fun unbindView(item: AboutCategory) {

        }

    }
}