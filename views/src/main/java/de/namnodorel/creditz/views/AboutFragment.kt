/*
 *    Copyright 2019 Felix Günther (Namnodorel)
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package de.namnodorel.creditz.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.GenericItemAdapter
import de.namnodorel.creditz.R
import de.namnodorel.creditz.views.AboutActivity.Companion.CREDITZ_CONFIG

class AboutFragment : Fragment() {

    private lateinit var dependencyInfo: Set<DependencyInfo>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        dependencyInfo = getDependencyInfo()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_about, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val config = arguments?.get(CREDITZ_CONFIG) as CreditzConfig? ?: CreditzConfig()

        val recyclerView = view.findViewById<RecyclerView>(R.id.content_view)

        val itemAdapter = GenericItemAdapter()
        val fastAdapter = FastAdapter.with(itemAdapter)

        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = fastAdapter

        if(!config.aboutTitle.isNullOrEmpty() || !config.aboutDescription.isNullOrEmpty() || !config.infoButtons.isNullOrEmpty()){
            itemAdapter.add(AppInfoHeader(config.aboutTitle, config.aboutDescription, config.infoButtons))
        }

        if(config.contributors.isNotEmpty()){

            itemAdapter.add(AboutCategory("Contributors"))

            for(contributor in config.contributors){

                itemAdapter.add(ContributorItem(contributor))

            }
        }

        if(config.groupMode == GroupMode.DIRECT_TRANSITIVE_MERGED){
            itemAdapter.add(
                dependencyInfo
                    .sortedBy { it.moduleName }
                    .map {
                        return@map DependencyListItem(it, config.linkToProjects)
                    }
            )

        }else{

            itemAdapter.add(AboutCategory("Used libraries"))

            itemAdapter.add(
                dependencyInfo
                    .filter { !it.transitive }
                    .sortedBy { it.moduleName }
                    .map {
                        return@map DependencyListItem(it, config.linkToProjects)
                    }
            )

            if(config.groupMode == GroupMode.DIRECT_TRANSITIVE_SEPERATE){

                itemAdapter.add(AboutCategory("Transitive used libraries"))

                itemAdapter.add(
                    dependencyInfo
                        .filter { it.transitive }
                        .sortedBy { it.moduleName }
                        .map {
                            return@map DependencyListItem(it, config.linkToProjects)
                        }
                )

            }
        }
    }

    private fun getDependencyInfo(): Set<DependencyInfo> {
        val storeClass = Class.forName("de.namnodorel.creditz.info.DependencyInfoStore")
        val infoFunction = storeClass.getMethod("buildInfo")

        val instance = storeClass.newInstance()
        val info = infoFunction.invoke(instance) as Set<DependencyInfo>

        return info
    }

}