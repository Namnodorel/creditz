/*
 *    Copyright 2019 Felix Günther (Namnodorel)
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package de.namnodorel.creditz.views

import android.app.SearchManager
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import com.google.android.material.card.MaterialCardView
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import de.namnodorel.creditz.R


class DependencyListItem(val info: DependencyInfo, val linkToProject: Boolean) : AbstractItem<DependencyListItem.ViewHolder>() {

    override val layoutRes: Int
        get() = R.layout.dependency_item

    override val type: Int
        get() = R.id.dependency_list_item

    override fun getViewHolder(v: View): ViewHolder = ViewHolder(v as MaterialCardView)


    class ViewHolder(private val view: MaterialCardView) : FastAdapter.ViewHolder<DependencyListItem>(view) {

        override fun bindView(item: DependencyListItem, payloads: List<Any>) {

            val info = item.info

            val titleTv = view.findViewById<TextView>(R.id.dependency_title)
            val authorTv = view.findViewById<TextView>(R.id.dependency_author)
            val descriptionTv = view.findViewById<TextView>(R.id.dependency_description)
            val licenseTv = view.findViewById<TextView>(R.id.dependency_license)
            val versionTv = view.findViewById<TextView>(R.id.dependency_version)

            var name = info.moduleName.replace(info.moduleGroup, "")
            if(!info.simpleName.isNullOrEmpty()){
                name = info.simpleName
            }

            var authors = info.authors
                .toList()
                .sorted()
                .foldRight(StringBuilder(), { author, currentStr ->
                    currentStr.append(author.trim() + ", ")
                })
                .dropLast(2)
                .replace("\\s+".toRegex(), " ")

            if(authors.isEmpty()){
                authors = info.moduleGroup
            }

            authors = "by $authors"

            val description = info.description
                ?.replace("\\s+".toRegex(), " ")
                ?.trim()

            var license = info.licenses
                .toList()
                .foldRight(StringBuilder()) { license, currentStr ->
                    currentStr.append(license.first + "; ")
                }
                .dropLast(2)
                .trim()
                .replace("\\s+".toRegex(), " ")

            if(license.isEmpty()){
                license = "No known license"
            }

            var version = info.moduleVersion
            if(!info.versionName.isNullOrEmpty()){
                version = info.versionName
            }

            titleTv.text = name
            authorTv.text = authors
            versionTv.text = version
            descriptionTv.text = description
            licenseTv.text = license

            val descriptionVisibility = if(description.isNullOrEmpty()) GONE else VISIBLE
            descriptionTv.visibility = descriptionVisibility
            val secondDivider = view.findViewById<View>(R.id.divider2)
            secondDivider.visibility = descriptionVisibility

            if(item.linkToProject){
                view.isClickable = true
                view.isFocusable = true

                view.setOnClickListener {

                    if(!info.website.isNullOrEmpty()){
                        try {
                            val intent = Intent(Intent.ACTION_VIEW)
                            intent.data = Uri.parse(info.website)
                            view.context.startActivity(intent)
                        }catch(ex: Exception){
                            Log.e("Creditz", "Unable to go to " + info.website)
                        }
                    }else{

                        val searchTerm: String
                        if(!info.simpleName.isNullOrEmpty()){
                            searchTerm = info.simpleName
                        }else{
                            searchTerm = info.moduleName
                        }

                        try {
                            val intent = Intent(Intent.ACTION_WEB_SEARCH)
                            intent.putExtra(SearchManager.QUERY, searchTerm)
                            view.context.startActivity(intent)
                        }catch(ex: Exception){
                            Log.e("Creditz", "Unable to search for $searchTerm")
                        }
                    }

                }
            }else{
                view.isClickable = false
                view.isFocusable = false
            }
        }

        override fun unbindView(item: DependencyListItem) {

        }
    }
}