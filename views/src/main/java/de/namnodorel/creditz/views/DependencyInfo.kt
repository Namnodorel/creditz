/*
 *    Copyright 2019 Felix Günther (Namnodorel)
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package de.namnodorel.creditz.views

data class DependencyInfo(
    val moduleGroup: String,
    val moduleName: String,
    val moduleVersion: String,
    val transitive: Boolean,
    val simpleName: String?,
    val versionName: String?,
    val description: String?,
    val website: String?,
    val authors: Set<String>,
    val licenses: Map<String, String>
)